#include "BrickedGenerator.h"

void MapQ::BrickedGenerator::int_generate()
{
    w_data.setBlock(0, 0, 0, MapQ::BlockType::Brick);
    w_data.setBlock(63, 0, 0, MapQ::BlockType::Red);
    w_data.setBlock(0, 63, 0, MapQ::BlockType::Green);
    w_data.setBlock(0, 0, 63, MapQ::BlockType::Blue);
}
