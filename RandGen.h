#pragma once
#include "RandMT.h"

namespace MapQ
{
    class RandGen;
}

class MapQ::RandGen
{
private:

    MapQ::RandMT mt;
    RandGen();

    static RandGen& instance()
    {
         static RandGen rg;
         return rg;
    }

public:

    template <typename T>
    static T get(const T min, const T max)
    {
        return instance().mt.get(min, max);
    }

    template <size_t SIZE>
    static std::array<uint8_t, SIZE> getRandomBytes()
    {
        return instance().mt.getRandomBytes<SIZE>();
    }

    RandGen(RandGen const&) = delete;
    void operator=(RandGen const&) = delete;
};
