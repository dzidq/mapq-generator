#include "BrickedGenerator.h"

int main()
{
    MapQ::BrickedGenerator m_gen;
    m_gen.generate();
    std::ofstream outfile("test.txt", std::ofstream::binary);
    m_gen.save(outfile);

    return 0;
}
