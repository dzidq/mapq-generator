#include "NBT.h"
#include <climits>

template <typename T>
static T swap_endian(T t)
{
    union
    {
        T t;
        unsigned char byte[sizeof(T)];
    } source, dest;
    source.t = t;
    for (size_t k = 0; k < sizeof(T); ++k)
    {
        dest.byte[k] = source.byte[sizeof(T) - k - 1];
    }
    return dest.t;
}

static void writeName(std::ostream& stream, const std::string& name)
{
    const uint16_t swap_size = swap_endian<uint16_t>(name.size());
    stream.write(reinterpret_cast<const char*>(&swap_size), sizeof(uint16_t));
    stream.write(name.c_str(), name.size());
}

void MapQ::writeEndTag(std::ostream& stream)
{
    stream << static_cast<char>(MapQ::TAG::End);
}

void MapQ::writeByteTag(std::ostream& stream, const std::string& name, const uint8_t value)
{
    stream << static_cast<char>(MapQ::TAG::Byte);
    writeName(stream, name);
    writeNamelessByteTag(stream, value);
}

void MapQ::writeShortTag(std::ostream& stream, const std::string& name, const uint16_t value)
{
    stream << static_cast<char>(MapQ::TAG::Short);
    writeName(stream, name);
    writeNamelessShortTag(stream, value);
}

void MapQ::writeIntTag(std::ostream& stream, const std::string& name, const uint32_t value)
{
    stream << static_cast<char>(MapQ::TAG::Int);
    writeName(stream, name);
    writeNamelessIntTag(stream, value);
}

void MapQ::writeLongTag(std::ostream& stream, const std::string& name, const uint64_t value)
{
    stream << static_cast<char>(MapQ::TAG::Long);
    writeName(stream, name);
    writeNamelessLongTag(stream, value);
}

void MapQ::writeFloatTag(std::ostream& stream, const std::string& name, const uint32_t value)
{
    stream << static_cast<char>(MapQ::TAG::Float);
    writeName(stream, name);
    writeNamelessFloatTag(stream, value);
}

void MapQ::writeDoubleTag(std::ostream& stream, const std::string& name, const uint64_t value)
{
    stream << static_cast<char>(MapQ::TAG::Double);
    writeName(stream, name);
    writeNamelessDoubleTag(stream, value);
}

void MapQ::writeByteArrayTag(std::ostream& stream, const std::string& name, const uint32_t size, const uint8_t* data)
{
    stream << static_cast<char>(MapQ::TAG::ByteArray);
    writeName(stream, name);
    writeNamelessByteArrayTag(stream, size, data);
}

void MapQ::writeStringTag(std::ostream& stream, const std::string& name, const std::string& str)
{
    stream << static_cast<char>(MapQ::TAG::ByteArray);
    writeName(stream, name);
    writeNamelessStringTag(stream, str);
}

void MapQ::writeListTag(std::ostream& stream, const std::string& name, const MapQ::TAG type, const uint32_t size)
{
    stream << static_cast<char>(MapQ::TAG::List);
    writeName(stream, name);
    writeNamelessListTag(stream, type, size);
}

void MapQ::writeCompoundTag(std::ostream& stream, const std::string& name)
{
    stream << static_cast<char>(MapQ::TAG::Compound);
    writeName(stream, name);
}

void MapQ::writeIntArrayTag(std::ostream& stream, const std::string& name, const uint32_t size, const uint32_t* data)
{
    stream << static_cast<char>(MapQ::TAG::IntArray);
    writeName(stream, name);
    writeNamelessIntArrayTag(stream, size, data);
}

void MapQ::writeLongArrayTag(std::ostream& stream, const std::string& name, const uint32_t size, const uint64_t* data)
{
    stream << static_cast<char>(MapQ::TAG::LongArray);
    writeName(stream, name);
    writeNamelessLongArrayTag(stream, size, data);
}

void MapQ::writeNamelessByteTag(std::ostream& stream, const uint8_t value)
{
    stream << static_cast<char>(value);
}

void MapQ::writeNamelessShortTag(std::ostream& stream, const uint16_t value)
{
    const uint16_t swap_value = swap_endian(value);
    stream.write(reinterpret_cast<const char*>(&swap_value), sizeof(uint16_t));
}

void MapQ::writeNamelessIntTag(std::ostream& stream, const uint32_t value)
{
    const uint32_t swap_value = swap_endian(value);
    stream.write(reinterpret_cast<const char*>(&swap_value), sizeof(uint32_t));
}

void MapQ::writeNamelessLongTag(std::ostream& stream, const uint64_t value)
{
    const uint64_t swap_value = swap_endian(value);
    stream.write(reinterpret_cast<const char*>(&swap_value), sizeof(uint64_t));
}

void MapQ::writeNamelessFloatTag(std::ostream& stream, const uint32_t value)
{
    writeNamelessIntTag(stream, value);
}

void MapQ::writeNamelessDoubleTag(std::ostream& stream, const uint64_t value)
{
    writeNamelessLongTag(stream, value);
}

void MapQ::writeNamelessByteArrayTag(std::ostream& stream, const uint32_t size, const uint8_t* data)
{
    uint32_t swap_size = swap_endian(size);
    stream.write(reinterpret_cast<const char*>(&swap_size), sizeof(uint32_t));
    if (data != nullptr)
    {
        stream.write(reinterpret_cast<const char*>(data), size);
    }
}

void MapQ::writeNamelessStringTag(std::ostream& stream, const std::string& str)
{
    uint16_t swap_size = swap_endian<uint16_t>(str.size());
    stream.write(reinterpret_cast<const char*>(&swap_size), sizeof(uint16_t));
    stream.write(reinterpret_cast<const char*>(str.c_str()), str.size());
}

void MapQ::writeNamelessListTag(std::ostream& stream, const MapQ::TAG type, const uint32_t size)
{
    stream << static_cast<char>(type);
    uint32_t swap_size = swap_endian(size);
    stream.write(reinterpret_cast<const char*>(&swap_size), sizeof(uint32_t));
}

void MapQ::writeNamelessIntArrayTag(std::ostream& stream, const uint32_t size, const uint32_t* data)
{
    uint32_t swap_size = swap_endian(size);
    stream.write(reinterpret_cast<const char*>(&swap_size), sizeof(uint32_t));
    if (data != nullptr)
    {
        uint32_t swap_int;
        for (uint32_t i = 0; i < size; ++i)
        {
            swap_int = swap_endian<uint32_t>(data[i]);
            stream.write(reinterpret_cast<const char*>(&swap_int), sizeof(uint32_t));
        }
    }
}

void MapQ::writeNamelessLongArrayTag(std::ostream& stream,const uint32_t size, const uint64_t* data)
{
    uint32_t swap_size = swap_endian(size);
    stream.write(reinterpret_cast<const char*>(&swap_size), sizeof(uint32_t));
    if (data != nullptr)
    {
        uint64_t swap_long;
        for (uint32_t i = 0; i < size; ++i)
        {
            swap_long = swap_endian<uint64_t>(data[i]);
            stream.write(reinterpret_cast<const char*>(&swap_long), sizeof(uint64_t));
        }
    }
}
