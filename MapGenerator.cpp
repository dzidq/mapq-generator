#include "MapGenerator.h"
#include "RandGen.h"

MapQ::MapGenerator::MapGenerator()
{
    random_seed = 0;
}

void MapQ::MapGenerator::setRandomUUID()
{
    w_data.setUUID(MapQ::RandGen::getRandomBytes<16>());
}

void MapQ::MapGenerator::generate(const MapQ::Vector3d size)
{
    generate(MapQ::RandGen::get<uint64_t>(std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max()), size);
}

void MapQ::MapGenerator::generate(const uint16_t x, const uint16_t y, const uint16_t z)
{
    generate(MapQ::RandGen::get<uint64_t>(std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max()), x, y, z);
}

void MapQ::MapGenerator::generate(const uint64_t seed, const MapQ::Vector3d size)
{
    generate(seed, size.x, size.y, size.z);
}

void MapQ::MapGenerator::generate(const uint64_t seed, const uint16_t x, const uint16_t y, const uint16_t z)
{
    random_seed = seed;
    w_data.resize(z, y, z);
    setRandomUUID();
    int_generate();
}

void MapQ::MapGenerator::save(std::ostream& stream) const
{
    MapQ::writeCompoundTag(stream, "ClassicWorld");
    MapQ::writeByteTag(stream, "FormatVersion", 1);
    stream << w_data;
    writeExtraNBT(stream);
    MapQ::writeCompoundTag(stream, "Metadata");
    MapQ::writeByteArrayTag(stream, "Seed", sizeof(random_seed), reinterpret_cast<const uint8_t*>(&random_seed));
    MapQ::writeEndTag(stream);
    MapQ::writeEndTag(stream);
}
