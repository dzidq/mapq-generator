#pragma once
#include "NBT.h"

namespace MapQ
{
    union Vector3d;
}

union MapQ::Vector3d
{
    uint16_t v[3];
    struct
    {
        uint16_t x;
        uint16_t y;
        uint16_t z;
    };
    Vector3d(): x(0), y(0), z(0){}
    Vector3d(const uint16_t _x, const uint16_t _y, const uint16_t _z): x(_x), y(_y), z(_z){}

    friend std::ostream& operator<< (std::ostream& stream, const MapQ::Vector3d& vec)
    {
        MapQ::writeShortTag(stream, "X", vec.x);
        MapQ::writeShortTag(stream, "Y", vec.y);
        MapQ::writeShortTag(stream, "Z", vec.z);
        return stream;
    }
};
