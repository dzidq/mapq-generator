#pragma once
#include "MTGenerator.h"

namespace MapQ
{
    class BrickedGenerator;
}

class MapQ::BrickedGenerator: public MapQ::MTGenerator
{
protected:

    virtual void int_generate() override;
};
