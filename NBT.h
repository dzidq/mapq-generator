#pragma once
#include <fstream>
#include <string>

namespace MapQ
{
    enum TAG: uint8_t;
}

enum MapQ::TAG: uint8_t
{
    End = 0,
    Byte,
    Short,
    Int,
    Long,
    Float,
    Double,
    ByteArray,
    String,
    List,
    Compound,
    IntArray,
    LongArray
};

namespace MapQ
{
    void writeEndTag(std::ostream& stream);
    void writeByteTag(std::ostream& stream, const std::string& name, const uint8_t value);
    void writeShortTag(std::ostream& stream, const std::string& name, const uint16_t value);
    void writeIntTag(std::ostream& stream, const std::string& name, const uint32_t value);
    void writeLongTag(std::ostream& stream, const std::string& name, const uint64_t value);
    void writeFloatTag(std::ostream& stream, const std::string& name, const uint32_t value);
    void writeDoubleTag(std::ostream& stream, const std::string& name, const uint64_t value);
    void writeByteArrayTag(std::ostream& stream, const std::string& name, const uint32_t size, const uint8_t* data);
    void writeStringTag(std::ostream& stream, const std::string& name, const std::string& str);
    void writeListTag(std::ostream& stream, const std::string& name, const TAG type, const uint32_t size);
    void writeCompoundTag(std::ostream& stream, const std::string& name);
    void writeIntArrayTag(std::ostream& stream, const std::string& name, const uint32_t size, const uint32_t* data);
    void writeLongArrayTag(std::ostream& stream, const std::string& name, const uint32_t size, const uint64_t* data);

    void writeNamelessByteTag(std::ostream& stream, const uint8_t value);
    void writeNamelessShortTag(std::ostream& stream, const uint16_t value);
    void writeNamelessIntTag(std::ostream& stream, const uint32_t value);
    void writeNamelessLongTag(std::ostream& stream, const uint64_t value);
    void writeNamelessFloatTag(std::ostream& stream, const uint32_t value);
    void writeNamelessDoubleTag(std::ostream& stream, const uint64_t value);
    void writeNamelessByteArrayTag(std::ostream& stream, const uint32_t size, const uint8_t* data);
    void writeNamelessStringTag(std::ostream& stream, const std::string& str);
    void writeNamelessListTag(std::ostream& stream, const TAG type, const uint32_t size);
    void writeNamelessIntArrayTag(std::ostream& stream, const uint32_t size, const uint32_t* data);
    void writeNamelessLongArrayTag(std::ostream& stream,const uint32_t size, const uint64_t* data);
}
