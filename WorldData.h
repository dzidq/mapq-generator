#pragma once
#include "Vector3d.h"
#include "SpawnData.h"
#include "BlockArray.h"
#include "BlockType.h"
#include <fstream>
#include <memory>
#include <array>

namespace MapQ
{
    class WorldData;
}

class MapQ::WorldData
{
private:

    MapQ::SpawnData spawn;
    std::array<uint8_t, 16> UUID;
    BlockArray b_array;

    void nullUUID();

public:

    WorldData();
    WorldData(const uint16_t x, const uint16_t y, const uint16_t z);
    WorldData(const MapQ::Vector3d m_size);
    void resize(const uint16_t x, const uint16_t y, const uint16_t z);
    void resize(const MapQ::Vector3d m_size);
    void setSpawn(const uint16_t x, const uint16_t y, const uint16_t z, const uint8_t h, const uint8_t p);
    void setSpawn(const MapQ::SpawnData s_data);
    void setSpawnPosition(const MapQ::Vector3d position);
    void setSpawnPosition(const uint16_t x, const uint16_t y, const uint16_t z);
    void setSpawnAngles(const MapQ::Vector2d angles);
    void setSpawnAngles(const uint8_t h, const uint8_t p);
    void setUUID(const std::array<uint8_t, 16> uuid);
    void setBlock(const uint16_t x, const uint16_t y, const uint16_t z, const MapQ::BlockType value);
    void setBlock(const MapQ::Vector3d position, const MapQ::BlockType value);

    MapQ::Vector3d getMapSize() const;
    MapQ::SpawnData getSpawnData() const;
    MapQ::Vector3d getSpawnPosition() const;
    MapQ::Vector2d getSpawnAngles() const;
    std::array<uint8_t, 16> getUUID() const;
    MapQ::BlockType getBlock(const uint16_t x, const uint16_t y, const uint16_t z) const;
    MapQ::BlockType getBlock(const MapQ::Vector3d position) const;

    friend std::ostream& operator<< (std::ostream& stream, const MapQ::WorldData& w_data);
};
