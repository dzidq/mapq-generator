#pragma once
#include "WorldData.h"

namespace MapQ
{
    class MapGenerator;
}

class MapQ::MapGenerator
{
private:

    void setRandomUUID();

protected:

    MapQ::WorldData w_data;
    uint64_t random_seed;

    virtual void int_generate() = 0;
    virtual void writeExtraNBT(std::ostream& stream) const {}

public:

    MapGenerator();

    void generate(const MapQ::Vector3d size = MapQ::Vector3d(64, 64, 64));
    void generate(const uint16_t x, const uint16_t y, const uint16_t z);
    void generate(const uint64_t seed, const MapQ::Vector3d size = MapQ::Vector3d(64, 64, 64));
    void generate(const uint64_t seed, const uint16_t x, const uint16_t y, const uint16_t z);
    void save(std::ostream& stream) const;
};
