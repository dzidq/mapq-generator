#pragma once
#include "MapGenerator.h"
#include "RandMT.h"

namespace MapQ
{
    class MTGenerator;
}

class MapQ::MTGenerator: public MapQ::MapGenerator
{
protected:

    MapQ::RandMT mt;
};
