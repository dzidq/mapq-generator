#pragma once
#include "BlockType.h"
#include "Vector3d.h"
#include <memory>

namespace MapQ
{
    class BlockArray;
}

class MapQ::BlockArray
{
private:

    MapQ::Vector3d size;
    std::unique_ptr<MapQ::BlockType[]> block_array;

    void int_resize();

public:

    BlockArray();
    BlockArray(const MapQ::Vector3d size);
    BlockArray(const uint8_t x, const uint8_t y, const uint8_t z);
    BlockArray(const MapQ::BlockArray& blocks);

    void resize(const MapQ::Vector3d size);
    void resize(const uint16_t x, const uint16_t y, const uint16_t z);

    MapQ::BlockType operator() (const uint16_t x, const uint16_t y, const uint16_t z) const;
    MapQ::BlockType operator() (const MapQ::Vector3d position) const;
    MapQ::BlockType& operator() (const uint16_t x, const uint16_t y, const uint16_t z);
    MapQ::BlockType& operator() (const MapQ::Vector3d position);

    MapQ::Vector3d getSize() const;
    const MapQ::BlockType* getRawPointer() const;
};
