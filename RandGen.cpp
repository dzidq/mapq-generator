#include "RandGen.h"
#include <chrono>

MapQ::RandGen::RandGen()
{
    std::chrono::high_resolution_clock::duration d;
    d = std::chrono::high_resolution_clock::now().time_since_epoch();
    mt.seed(d.count());
}
