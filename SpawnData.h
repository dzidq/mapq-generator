#pragma once
#include "Vector3d.h"
#include "Vector2d.h"
#include <fstream>

namespace MapQ
{
    class SpawnData;
}

struct MapQ::SpawnData
{
    MapQ::Vector3d position;
    MapQ::Vector2d angles;

    SpawnData(){}
    SpawnData(const MapQ::Vector3d pos, const MapQ::Vector2d ang): position(pos), angles(ang){}
    SpawnData(const MapQ::Vector3d pos, const uint8_t h, const uint8_t p): position(pos), angles(h, p){}
    SpawnData(const uint16_t x, const uint16_t y, const uint16_t z, const MapQ::Vector2d ang): position(x, y, z), angles(ang){}
    SpawnData(const uint16_t x, const uint16_t y, const uint16_t z, const uint8_t h, const uint8_t p): position(x, y, z), angles(h, p){}

    friend std::ostream& operator<< (std::ostream& stream, const MapQ::SpawnData& s_data)
    {
        MapQ::writeCompoundTag(stream, "Spawn");
        stream << s_data.position << s_data.angles;
        MapQ::writeEndTag(stream);
        return stream;
    }
};
