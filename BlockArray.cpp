#include "BlockArray.h"

MapQ::BlockArray::BlockArray(): size(0, 0, 0){}

MapQ::BlockArray::BlockArray(const MapQ::Vector3d s)
{
    resize(s);
}

MapQ::BlockArray::BlockArray(const uint8_t x, const uint8_t y, const uint8_t z)
{
    resize(x, y, z);
}

MapQ::BlockArray::BlockArray(const MapQ::BlockArray& blocks)
{
    for (uint16_t y = 0; y < size.y; ++y)
    {
        for (uint16_t z = 0; z < size.z; ++z)
        {
            for (uint16_t x = 0; x < size.x; ++x)
            {
                (*this)(x, y, z) = blocks(x, y, z);
            }
        }
    }
}

void MapQ::BlockArray::int_resize()
{
    block_array.reset(new MapQ::BlockType[size.x * size.y * size.z]()); // Assuming MapQ::BlockType::Air == 0
}

void MapQ::BlockArray::resize(const MapQ::Vector3d s)
{
    size = s;
    int_resize();
}

void MapQ::BlockArray::resize(const uint16_t x, const uint16_t y, const uint16_t z)
{
    size = MapQ::Vector3d(x, y, z);
    int_resize();
}

const MapQ::BlockType* MapQ::BlockArray::getRawPointer() const
{
    return block_array.get();
}

MapQ::BlockType MapQ::BlockArray::operator() (const uint16_t x, const uint16_t y, const uint16_t z) const
{
    return block_array[x + z*size.x + y*size.x*size.z];
}

MapQ::BlockType MapQ::BlockArray::operator() (const MapQ::Vector3d pos) const
{
    return block_array[pos.x + pos.z*size.x + pos.y*size.x*size.z];
}

MapQ::BlockType& MapQ::BlockArray::operator() (const uint16_t x, const uint16_t y, const uint16_t z)
{
    return block_array[x + z*size.x + y*size.x*size.z];
}

MapQ::BlockType& MapQ::BlockArray::operator() (const MapQ::Vector3d pos)
{
    return block_array[pos.x + pos.z*size.x + pos.y*size.x*size.z];
}

MapQ::Vector3d MapQ::BlockArray::getSize() const
{
    return size;
}
