#pragma once
#include "NBT.h"

namespace MapQ
{
    union Vector2d;
}

union MapQ::Vector2d
{
    uint8_t v[2];
    struct
    {
        uint8_t h; // Heading (yaw)
        uint8_t p; // Pitch
    };
    Vector2d(): h(0), p(0){}
    Vector2d(const uint8_t _h, const uint8_t _p): h(_h), p(_p){}

    friend std::ostream& operator<< (std::ostream& stream, const MapQ::Vector2d& vec)
    {
        MapQ::writeByteTag(stream, "H", vec.h);
        MapQ::writeByteTag(stream, "P", vec.p);
        return stream;
    }
};

