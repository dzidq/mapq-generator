#include "WorldData.h"

MapQ::WorldData::WorldData()
{
    nullUUID();
}

MapQ::WorldData::WorldData(const uint16_t x, const uint16_t y, const uint16_t z): b_array(x, y, z)
{
    nullUUID();
}

MapQ::WorldData::WorldData(const Vector3d m_size): b_array(m_size)
{
    nullUUID();
}

void MapQ::WorldData::resize(const uint16_t x, const uint16_t y, const uint16_t z)
{
    b_array.resize(x, y, z);
}

void MapQ::WorldData::resize(const Vector3d m_size)
{
    b_array.resize(m_size);
}

void MapQ::WorldData::setSpawn(const uint16_t x, const uint16_t y, const uint16_t z, const uint8_t h, const uint8_t p)
{
    spawn = SpawnData(x, y, z, h, p);
}

void MapQ::WorldData::setSpawn(const SpawnData s_data)
{
    spawn = s_data;
}

void MapQ::WorldData::setSpawnPosition(const Vector3d pos)
{
    spawn.position = pos;
}

void MapQ::WorldData::setSpawnPosition(const uint16_t x, const uint16_t y, const uint16_t z)
{
    spawn.position = Vector3d(x, y, z);
}

void MapQ::WorldData::setSpawnAngles(const MapQ::Vector2d ang)
{
    spawn.angles = ang;
}

void MapQ::WorldData::setSpawnAngles(const uint8_t h, const uint8_t p)
{
    spawn.angles = Vector2d(h, p);
}

void MapQ::WorldData::setUUID(const std::array<uint8_t, 16> uuid)
{
    UUID = uuid;
}

void MapQ::WorldData::setBlock(const uint16_t x, const uint16_t y, const uint16_t z, const MapQ::BlockType value)
{
    b_array(x, y, z) = value;
}

void MapQ::WorldData::setBlock(const MapQ::Vector3d pos, const MapQ::BlockType value)
{
    b_array(pos) = value;
}

MapQ::Vector3d MapQ::WorldData::getMapSize() const
{
    return b_array.getSize();
}

MapQ::SpawnData MapQ::WorldData::getSpawnData() const
{
    return spawn;
}

MapQ::Vector3d MapQ::WorldData::getSpawnPosition() const
{
    return spawn.position;
}

MapQ::Vector2d MapQ::WorldData::getSpawnAngles() const
{
    return spawn.angles;
}

std::array<uint8_t, 16> MapQ::WorldData::getUUID() const
{
    return UUID;
}

MapQ::BlockType MapQ::WorldData::getBlock(const uint16_t x, const uint16_t y, const uint16_t z) const
{
    return b_array(x, y, z);
}

MapQ::BlockType MapQ::WorldData::getBlock(const Vector3d pos) const
{
    return b_array(pos);
}

void MapQ::WorldData::nullUUID()
{
    UUID.fill(0x00);
}

std::ostream& MapQ::operator<< (std::ostream& stream, const MapQ::WorldData& w_data)
{
    const MapQ::Vector3d map_size = w_data.getMapSize();
    stream << map_size << w_data.spawn;
    MapQ::writeByteArrayTag(stream, "UUID", 16, w_data.UUID.data());
    MapQ::writeByteArrayTag(stream, "BlockArray", map_size.x * map_size.y * map_size.z, reinterpret_cast<const uint8_t*>(w_data.b_array.getRawPointer()));
    return stream;
}
