#pragma once
#include <random>
#include <array>

namespace MapQ
{
    class RandMT;
}

class MapQ::RandMT: public std::mt19937_64
{
public:

    template <typename T>
    T get(const T min, const T max)
    {
        if (min <= max)
        {
            std::uniform_int_distribution<T> dist(min, max);
            return dist(*this);
        }
        else
        {
            std::uniform_int_distribution<T> dist(max, min);
            return dist(*this);
        }
    }

    template <size_t SIZE>
    std::array<uint8_t, SIZE> getRandomBytes()
    {
        union
        {
            uint64_t val;
            uint8_t byte[SIZE];
        } rand;
        std::array<uint8_t, SIZE> arr;
        bool filled = false;
        for (unsigned i = 0; !filled; ++i)
        {
            rand.val = get<uint64_t>(std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max());
            for (unsigned j = 0; j < sizeof(uint8_t); ++j)
            {
                unsigned byte_num = sizeof(uint8_t)*i+j;
                if (byte_num == SIZE)
                {
                    filled = true;
                    break;
                }
                arr[byte_num] = rand.byte[j];
            }
            if (filled)
            {
                break;
            }
        }
        return arr;
    }
};
