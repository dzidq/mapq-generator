#pragma once
#include <cstdint>

namespace MapQ
{
    enum BlockType: uint8_t;
}

enum MapQ::BlockType: uint8_t
{
    Air = 0,
    Stone = 1,
    Grass = 2,
    Dirt = 3,
    Cobblestone = 4,
    Wood = 5,
    Sapling = 6,
    Bedrock = 7,
    Active_Water = 8,
    Water = 9,
    Active_Lava = 10,
    Lava = 11,
    Sand = 12,
    Gravel = 13,
    Gold_Ore = 14,
    Iron_Ore = 15,
    Coal_Ore = 16,
    Log = 17,
    Leaves = 18,
    Sponge = 19,
    Glass = 20,
    Red = 21,
    Orange = 22,
    Yellow = 23,
    Lime = 24,
    Green = 25,
    Teal = 26,
    Aqua = 27,
    Cyan = 28,
    Blue = 29,
    Indigo = 30,
    Violet = 31,
    Magenta = 32,
    Pink = 33,
    Black = 34,
    Gray = 35,
    White = 36,
    Dandelion = 37,
    Rose = 38,
    Brown_Shroom = 39,
    Red_Shroom = 40,
    Gold = 41,
    Iron = 42,
    DoubleSlab = 43,
    Slab = 44,
    Brick = 45,
    TNT = 46,
    BookShelf = 47,
    MossyRocks = 48,
    Obsidian = 49,
    CobblestoneSlab = 50,
    Rope = 51,
    SandStone = 52,
    Snow = 53,
    Fire = 54,
    LightPink = 55,
    ForestGreen = 56,
    Brown = 57,
    DeepBlue = 58,
    Turquoise = 59,
    Ice = 60,
    CeramicTile = 61,
    MagmaBlock = 62,
    Pillar = 63,
    Crate = 64,
    StoneBrick = 65
};
